# -*- coding: utf-8 -*-
from setuptools import setup

requirements = [
    'flask',
    'python-dotenv',
    # 'flask-simplelogin'​,
    # 'flask-admin'​,
    # 'flask-wtf'​,
    # 'flask-restful'​,
    # 'flask-pytest'​,
    # 'flask-bootstrap'​,
    # 'flasgger'
]


setup(
    name='cms',
    version='1.0.0',
    description='Colaborasol CMS',
    packages=['webapp'],
    include_package_data=True,
    install_requires=requirements
)
