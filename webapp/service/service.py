from flask import redirect, url_for, flash, render_template, abort
from parse_rest.query import QueryResourceDoesNotExist


class Service():

    def post_generic_object(self, form, target_obj, return_page):
        ERROR_MSG = 'O formulário contem erros'
        SUCCESS_MSG = 'Registro atualizado com sucesso'

        if form.validate():
            form.populate_obj(target_obj)
            target_obj.save()
            flash(SUCCESS_MSG, 'success')
            return redirect(url_for(return_page))
        else:
            flash(ERROR_MSG, 'error')

    @classmethod
    def generic_edit_view(self, request, db_object, object_id, typeForm, context, html_edit, html_index):
        try:
            target_obj = db_object.Query.get(objectId=object_id)
        except QueryResourceDoesNotExist:
            abort(404, 'Registo não encontrado')

        form = typeForm(request.form, obj=target_obj)
        context.update({
            'form': form,
            'target_obj': target_obj,
        })
        if request.method == 'POST':
            return self.post_generic_object(self, form, target_obj, html_index)
        else:
            return render_template(html_edit, **context)
