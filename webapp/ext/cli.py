import click
from webapp.ext.login import create_user


def configure(app):
    """Attach new commands to app"""

    @app.cli.command()
    @click.option('--text', '-t', required=True)
    def echo(text):
        """Echo test for cli.py"""

        click.echo(text)

    @app.cli.command()
    @click.option('--username', '-u', required=True)
    @click.option('--password', '-p', required=True)
    def adduser(username, password):
        """Creates a new admin user"""
        user = create_user(username, password)
        click.echo(f"{user.inserted_id} cadastrado com sucesso!")
