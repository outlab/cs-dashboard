# -*- coding: utf-8 -*-
import os
from parse_rest.connection import register
from dotenv import load_dotenv
from pathlib import Path

from flask import Flask
from webapp.ext import cli, db, login
from webapp.blueprints import webui

# loads .env file
load_dotenv()
# loads .flask_env file
flask_env_path = Path('.') / '.flaskenv'
load_dotenv(dotenv_path=flask_env_path, verbose=True)


def register_parse_api(app):
    APPLICATION_ID = app.config['PARSE_APPLICATION_ID']
    REST_API_KEY = app.config['PARSE_REST_API_KEY']
    MASTER_KEY = app.config['PARSE_MASTER_KEY']
    register(APPLICATION_ID, REST_API_KEY, master_key=MASTER_KEY)


def create_app(**kwargs):
    """
    Creates a new flask app using the factory pattern
    """

    app = Flask(__name__,
                static_url_path='/static',
                static_folder='static')
    app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY', '4d43d5f10bf64de697898f78590e4d56')
    custom_settings = {
        'PARSE_API_ROOT': os.environ.get('PARSE_API_ROOT'),
        'PARSE_APPLICATION_ID': os.environ.get('PARSE_APPLICATION_ID'),
        'PARSE_REST_API_KEY': os.environ.get('PARSE_REST_API_KEY'),
        'PARSE_MASTER_KEY': os.environ.get('PARSE_MASTER_KEY'),
    }
    app.config.update(**custom_settings)
    app.config.update(kwargs)
    # Parse API
    register_parse_api(app)
    # extensões
    cli.configure(app)  # <-- registro dinâmico de extensões
    db.configure(app)  # <-- registro dinâmico de extensões
    login.configure(app)  # <-- registro dinâmico de extensões
    # blueprints
    webui.configure(app)  # <-- registro dinâmico de blueprints

    return app
