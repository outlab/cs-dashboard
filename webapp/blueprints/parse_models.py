# -*- coding: utf-8 -*-
from parse_rest.datatypes import Object, Relation, Pointer


class Suppliers(Object):
    pass


class Review(Object):
    """
    Review (aka Feedback) model
    """
    pass


class Quotations(Object):
    pass


class Qualifications(Object):
    pass


class JobType(Object):
    pass


class Slides(Object):
    pass


class FAQ(Object):
    pass


class LegalTexts(Object):
    pass


class Location(Object):
    pass
