# -*- coding: utf-8 -*-

from flask_simplelogin import login_required

from ..service.service import Service
from flask import request, Blueprint, render_template, redirect, url_for, flash
from parse_rest.user import User
from parse_rest.core import ResourceRequestNotFound
from parse_rest.query import QueryResourceDoesNotExist
from .parse_models import (
    Suppliers,
    Review,
    Quotations,
    Qualifications,
    JobType,
    Slides,
    FAQ,
    LegalTexts,
    Location,
)
from .forms import (
    LocationForm, SupplierForm, FAQForm, LegalTextsForm, ReviewForm, QuotationsForm, UserForm, QualificationsForm,
    JobTypeForm, SlidesForm
)

bp = Blueprint('webui', __name__)
MAX_PAGE_SIZE = 20

menu_titles = {
    'main': {
        'home': {'title': 'Home', 'icon_name': 'home'},
        'users': {'title': 'Usuários', 'class_name': User, 'icon_name': 'user'},
        'users': {'title': 'Usuários', 'icon_name': 'user'},
        'suppliers': {'title': 'Colaboradores', 'class_name': Suppliers, 'icon_name': 'truck'},
        'feedbacks': {'title': 'Feedbacks / Avaliações', 'class_name': Review, 'icon_name': 'star'},
        'quotations': {'title': 'Orçamentos', 'class_name': Quotations, 'icon_name': 'message-square'},
        'qualifications': {'title': 'Qualificações', 'class_name': Qualifications, 'icon_name': 'book-open'},
        'jobtype': {'title': 'Tipos de Serviço', 'class_name': JobType, 'icon_name': 'box'},
        'slides': {'title': 'Slides', 'class_name': Slides, 'icon_name': 'layers'},
        'location': {'title': 'Locations', 'class_name': Location, 'icon_name': 'map-pin'},
    },
    'legal': {
        'faqs': {'title': 'FAQ', 'class_name': FAQ, 'icon_name': 'file-text'},
        'tos': {'title': 'Termos e Condições', 'class_name': LegalTexts, 'icon_name': 'file-text'},
    },
}

msg = {
    'obj_not_found': 'Item não encontrado!',
    'page_not_found': 'A página solicitada não existe!',
}

general_context = {'main_menu_titles': menu_titles['main'], 'legal_texts_titles': menu_titles['legal'],}


@bp.route('/')
@login_required
def home():
    # TODO: resolver os titulos do sidebar aqui!
    latest_feedbacks = Review.Query.all().order_by('-createdAt').limit(10)
    recent_user_signups = User.Query.all().order_by('-createdAt').limit(10)
    context = {
        'latest_feedbacks': latest_feedbacks,
        'recent_user_signups': recent_user_signups,
    }
    context.update(general_context)
    return render_template('home.html', **context)


@bp.route('/users')
@login_required
def users_index():
    page_title = menu_titles['main']['users']['title']
    page_size = request.args.get('page_count', None, type=int)
    if page_size is None or page_size > MAX_PAGE_SIZE:
        page_size = 15

    all_users = User.Query.all().order_by('-createdAt').limit(page_size)
    context = {
        'all_users': all_users,
        'page_title': page_title,
    }
    context.update(general_context)
    return render_template('users/index.html', **context)


@bp.route('/users/<string:object_id>/edit/', methods=['GET', 'POST'])
@login_required
def users_edit(object_id):
    context = {
        'page_title': menu_titles['main']['users']['title'],
    }
    context.update(general_context)
    return Service.generic_edit_view(request, User, object_id, UserForm, context, 'users/edit.html',
                                     '.users_index')

@bp.route('/suppliers')
@login_required
def suppliers_index():
    page_size = request.args.get('page_count', None, type=int)
    if page_size is None or page_size > MAX_PAGE_SIZE:
        page_size = 15

    all_suppliers = Suppliers.Query.all().order_by('-createdAt').limit(page_size)
    context = {
        'all_suppliers': all_suppliers,
        'page_title': menu_titles['main']['suppliers']['title'],
    }
    context.update(general_context)
    return render_template('suppliers/index.html', **context)


@bp.route('/suppliers/<string:object_id>/edit/', methods=['GET', 'POST'])
@login_required
def supplier_edit(object_id):
    context = {
        'page_title': menu_titles['main']['suppliers']['title'],
    }
    context.update(general_context)
    return Service.generic_edit_view(request, Suppliers, object_id, SupplierForm, context, 'suppliers/edit.html', '.suppliers_index')


@bp.route('/feedback/<string:object_id>/edit/', methods=['GET', 'POST'])
@login_required
def feedback_edit(object_id):
    context = {
        'page_title': menu_titles['main']['feedbacks']['title'],
    }
    context.update(general_context)
    return Service.generic_edit_view(request, Review, object_id, ReviewForm, context, 'feedbacks/edit.html', '.feedbacks_index')


@bp.route('/feedbacks')
@login_required
def feedbacks_index():
    page_size = request.args.get('page_count', None, type=int)
    if page_size is None or page_size > MAX_PAGE_SIZE:
        page_size = 15

    all_feedbacks = Review.Query.all().order_by('-createdAt').limit(page_size)

    context = {
        'all_feedbacks': all_feedbacks,
        'page_title': menu_titles['main']['feedbacks']['title'],
    }
    context.update(general_context)
    return render_template('feedbacks/index.html', **context)


@bp.route('/quotations')
@login_required
def quotations_index():
    page_size = request.args.get('page_count', None, type=int)
    if page_size is None or page_size > MAX_PAGE_SIZE:
        page_size = 15

    # TODO: adicionar variável da query para encontrar se existe feedback para o orçamento em questão e printar o id com link p/ feedback
    all_quotations = Quotations.Query.all().order_by('-createdAt').limit(page_size)

    context = {
        'all_quotations': all_quotations,
        'page_title': menu_titles['main']['quotations']['title'],
    }
    context.update(general_context)
    return render_template('quotations/index.html', **context)


@bp.route('/quotations/<string:object_id>/edit/', methods=['GET', 'POST'])
@login_required
def quotations_edit(object_id):
    context = {
        'page_title': menu_titles['main']['quotations']['title'],
    }
    context.update(general_context)
    return Service.generic_edit_view(request, Quotations, object_id, QuotationsForm, context, 'quotations/edit.html', '.quotations_index')


@bp.route('/qualifications')
@login_required
def qualifications_index():
    page_size = request.args.get('page_count', None, type=int)
    if page_size is None or page_size > MAX_PAGE_SIZE:
        page_size = 15

    # TODO: adicionar variável da query para encontrar se existe feedback para o orçamento em questão e printar o id com link p/ feedback
    all_qualifications = Qualifications.Query.all().order_by('-createdAt').limit(page_size)

    context = {
        'all_qualifications': all_qualifications,
        'page_title': menu_titles['main']['qualifications']['title'],
    }
    context.update(general_context)
    return render_template('qualifications/index.html', **context)


@bp.route('/qualifications/<string:object_id>/edit/', methods=['GET', 'POST'])
@login_required
def qualifications_edit(object_id):
    context = {
        'page_title': menu_titles['main']['qualifications']['title'],
    }
    context.update(general_context)
    return Service.generic_edit_view(request, Qualifications, object_id, QualificationsForm, context, 'qualifications/edit.html', '.qualifications_index')

@bp.route('/qualifications/add/', methods=['GET', 'POST'])
@login_required
def qualifications_add():
    form = QualificationsForm(request.form)
    context = {
        'form': form,
        'page_title': menu_titles['main']['qualifications']['title'],
    }
    context.update(general_context)
    if request.method == 'POST':
        if form.validate():
            new_obj = Qualifications()
            if not hasattr(new_obj, 'cityId'):
                city_id = Qualifications.Query.all().count() + 1
                new_obj.cityId = city_id
            form.populate_obj(new_obj)
            new_obj.save()
            flash('Registro criado com sucesso', 'success')
            return redirect(url_for('.qualifications_index'))
        else:
            # form com erros -> mensagem para o usuário
            flash('O formulário contem erros', 'error')
    else:
        # é GET -> mostramos os dados no form
        return render_template('qualifications/add.html', **context)

@bp.route('/jobtype')
@login_required
def jobtype_index():
    page_size = request.args.get('page_count', None, type=int)
    if page_size is None or page_size > MAX_PAGE_SIZE:
        page_size = 15

    # TODO: adicionar variável da query para encontrar se existe feedback para o orçamento em questão e printar o id com link p/ feedback
    all_jobtype = JobType.Query.all().order_by('-createdAt').limit(page_size)

    context = {
        'all_jobtype': all_jobtype,
        'page_title':  menu_titles['main']['jobtype']['title'],
    }
    context.update(general_context)
    return render_template('jobtype/index.html', **context)


@bp.route('/jobtype/<string:object_id>/edit/', methods=['GET', 'POST'])
@login_required
def jobtype_edit(object_id):
    context = {
        'page_title': menu_titles['main']['jobtype']['title'],
    }
    context.update(general_context)
    return Service.generic_edit_view(request, JobType, object_id, JobTypeForm, context, 'jobtype/edit.html', '.jobtype_index')

@bp.route('/jobtype/add/', methods=['GET', 'POST'])
@login_required
def jobtype_add():
    form = JobTypeForm(request.form)
    context = {
        'form': form,
        'page_title': menu_titles['main']['jobtype']['title'],
    }
    context.update(general_context)
    if request.method == 'POST':
        if form.validate():
            new_obj = JobType()
            if not hasattr(new_obj, 'cityId'):
                city_id = JobType.Query.all().count() + 1
                new_obj.cityId = city_id
            form.populate_obj(new_obj)
            new_obj.save()
            flash('Registro criado com sucesso', 'success')
            return redirect(url_for('.jobtype_index'))
        else:
            # form com erros -> mensagem para o usuário
            flash('O formulário contem erros', 'error')
    else:
        # é GET -> mostramos os dados no form
        return render_template('jobtype/add.html', **context)

@bp.route('/slides')
@login_required
def slides_index():
    page_size = request.args.get('page_count', None, type=int)
    if page_size is None or page_size > MAX_PAGE_SIZE:
        page_size = 15

    # TODO: adicionar variável da query para encontrar se existe feedback para o orçamento em questão e printar o id com link p/ feedback
    all_slides = Slides.Query.all().order_by('-createdAt').limit(page_size)
    context = {
        'all_slides': all_slides,
        'page_title': menu_titles['main']['slides']['title'],
    }
    context.update(general_context)
    return render_template('slides/index.html', **context)


@bp.route('/slides/<string:object_id>/edit/', methods=['GET', 'POST'])
@login_required
def slides_edit(object_id):
    context = {
        'page_title': menu_titles['main']['slides']['title'],
    }
    context.update(general_context)
    return Service.generic_edit_view(request, Slides, object_id, SlidesForm, context, 'slides/edit.html',
                                     '.slides_index')

@bp.route('/slides/add/', methods=['GET', 'POST'])
@login_required
def slides_add():
    form = SlidesForm(request.form)
    context = {
        'form': form,
        'page_title': menu_titles['main']['slides']['title'],
    }
    context.update(general_context)
    if request.method == 'POST':
        if form.validate():
            new_obj = Slides()
            if not hasattr(new_obj, 'cityId'):
                city_id = Slides.Query.all().count() + 1
                new_obj.cityId = city_id
            form.populate_obj(new_obj)
            new_obj.save()
            flash('Registro criado com sucesso', 'success')
            return redirect(url_for('.slides_index'))
        else:
            # form com erros -> mensagem para o usuário
            flash('O formulário contem erros', 'error')
    else:
        # é GET -> mostramos os dados no form
        return render_template('slides/add.html', **context)


@bp.route('/location')
@login_required
def location_index():
    page_size = request.args.get('page_count', None, type=int)
    if page_size is None or page_size > MAX_PAGE_SIZE:
        page_size = 15

    # TODO: limitar a quantidade de caracteres que aparecem no HTML no campo text
    all_locations = Location.Query.all().order_by('-createdAt').limit(page_size)
    context = {
        'all_locations': all_locations,
        'page_title': menu_titles['main']['location']['title'],
    }
    context.update(general_context)
    return render_template('location/index.html', **context)


@bp.route('/location/<string:object_id>/edit/', methods=['GET', 'POST'])
@login_required
def location_edit(object_id):
    context = {
        'page_title': menu_titles['main']['location']['title'],
    }
    context.update(general_context)
    return Service.generic_edit_view(request, Location, object_id, LocationForm, context, 'location/edit.html',
                                     '.location_index')


@bp.route('/location/add/', methods=['GET', 'POST'])
@login_required
def location_add():
    form = LocationForm(request.form)
    context = {
        'form': form,
        'page_title': menu_titles['main']['location']['title'],
    }
    context.update(general_context)
    if request.method == 'POST':
        if form.validate():
            new_obj = Location()
            if not hasattr(new_obj, 'cityId'):
                city_id = Location.Query.all().count() + 1
                new_obj.cityId = city_id
            form.populate_obj(new_obj)
            new_obj.save()
            flash('Registro criado com sucesso', 'success')
            return redirect(url_for('.location_index'))
        else:
            # form com erros -> mensagem para o usuário
            flash('O formulário contem erros', 'error')
    else:
        # é GET -> mostramos os dados no form
        return render_template('location/add.html', **context)


# ## LEGAL TEXTS


@bp.route('/faq/<string:object_id>/edit/', methods=['GET', 'POST'])
@login_required
def faq_edit(object_id):
    context = {
        'page_title': menu_titles['legal']['faqs']['title'],
    }
    context.update(general_context)
    return Service.generic_edit_view(request, FAQ, object_id, FAQForm, context, 'faqs/edit.html',
                                     '.faqs_index')


@bp.route('/faqs')
@login_required
def faqs_index():
    page_size = request.args.get('page_count', None, type=int)
    if page_size is None or page_size > MAX_PAGE_SIZE:
        page_size = 15

    # TODO: limitar a quantidade de caracteres que aparecem no HTML no campo text
    all_faqs = FAQ.Query.all().order_by('ranking').limit(page_size)
    context = {
        'all_faqs': all_faqs,
        'page_title': menu_titles['legal']['faqs']['title'],
    }
    context.update(general_context)
    return render_template('faqs/index.html', **context)


@bp.route('/tos')
@login_required
def tos_index():
    page_size = request.args.get('page_count', None, type=int)
    if page_size is None or page_size > MAX_PAGE_SIZE:
        page_size = 15

    # TODO: limitar a quantidade de caracteres que aparecem no HTML no campo text
    all_tos = LegalTexts.Query.all().order_by('-createdAt').limit(page_size)
    context = {
        'all_tos': all_tos,
        'page_title': menu_titles['legal']['tos']['title'],
    }
    context.update(general_context)
    return render_template('tos/index.html', **context)


@bp.route('/tos/<string:object_id>/edit/', methods=['GET', 'POST'])
@login_required
def tos_edit(object_id):
    context = {
        'page_title': menu_titles['legal']['tos']['title'],
    }
    context.update(general_context)
    return Service.generic_edit_view(request, LegalTexts, object_id, LegalTextsForm, context, 'tos/edit.html',
                                     '.tos_index')

# ## GLOBAL ROUTES / ACTIONS


@bp.route('/<string:model_name>/<string:object_id>/delete')
@login_required
def object_delete(model_name, object_id):

    model_class = None

    main_models_list = list(menu_titles['main'].keys())
    legal_models_list = list(menu_titles['legal'].keys())

    if model_name in main_models_list:
        model_class = menu_titles['main'][model_name]['class_name']
    elif model_name in legal_models_list:
        model_class = menu_titles['legal'][model_name]['class_name']
    else:
        flash(msg['page_not_found'], 'error')
        return redirect(url_for('webui.home'))

    try:
        object_to_delete = model_class.Query.get(objectId=object_id)
        object_to_delete.delete()
    except (ResourceRequestNotFound, QueryResourceDoesNotExist):
        flash(msg['obj_not_found'], 'error')

    return redirect(url_for('webui.%s_index' % model_name))


def configure(app):
    """
    Register blueprint to the app
    """
    app.register_blueprint(bp)
