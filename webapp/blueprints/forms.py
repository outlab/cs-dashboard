from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, TextAreaField, BooleanField
from wtforms.validators import DataRequired


class LocationForm(FlaskForm):
    city = StringField('Cidade', validators=[DataRequired()], description='nome da cidade')
    state = StringField('Estado/UF', validators=[DataRequired()], description='UF do estado')


class SupplierForm(FlaskForm):
    razaoSocial = StringField('Razão Social', validators=[DataRequired()], description='Razão Social')
    cnpj = StringField('CNPJ', description='CNPJ')
    supplierField = StringField('Área de Atuação/Formação', validators=[DataRequired()], description='Área de Atuação/Formação')
    userId = StringField('Usuário', description='Usuário',)
    objectId = StringField('Id', description='Id', )
    companyType = StringField('Tipo de Empresa', validators=[DataRequired()], description='Tipo de Empresa')
    xp = IntegerField('Experiência', validators=[DataRequired()], description='Experiência')
    finishedJobs = IntegerField('Trabalhos Concluídos', validators=[DataRequired()], description='Trabalhos Concluídos')


class FAQForm(FlaskForm):
    answer = TextAreaField('Resposta', validators=[DataRequired()], description='Resposta')
    question = StringField('Pergunta', validators=[DataRequired()], description='Pergunta')
    ranking = IntegerField('ranking', validators=[DataRequired()], description='ranking')
    public = BooleanField('Público', description='Público')


class LegalTextsForm(FlaskForm):
    text = TextAreaField('Texto/HTML', validators=[DataRequired()], description='Texto/HTML')
    title = StringField('Título', validators=[DataRequired()], description='Título')
    name = StringField('Nome Código', validators=[DataRequired()], description='Nome Código')

# 'senderId': <User:igor@outlab.rio (Id rueh8Ja2R1)>,
# 'recipientId': <Suppliers:hpBaThwCRL>,


class QuotationsForm(FlaskForm):
    message = TextAreaField('Mensagem', validators=[DataRequired()], description='Mensagem')
    statusName = StringField('Status', validators=[DataRequired()], description='Status')


class ReviewForm(FlaskForm):
    message = StringField('Mensagem', description='Mensagem')


class QualificationsForm(FlaskForm):
    name = StringField('Nome', validators=[DataRequired()], description='Nome')
    qualId = StringField('Abreviação / ID', validators=[DataRequired()], description='Abreviação / ID')


class JobTypeForm(FlaskForm):
    name = StringField('Nome', validators=[DataRequired()], description='Nome')
    typeId = StringField('Abreviação / ID', validators=[DataRequired()], description='Abreviação / ID')
    typeJob = StringField('Tipo de Serviço', validators=[DataRequired()], description='Tipo de Serviço')


class SlidesForm(FlaskForm):
    title = StringField('Título', validators=[DataRequired()], description='Título')
    icon = StringField('Ícone', validators=[DataRequired()], description='Ícone')
    text = StringField('Texto', description='Texto')


class UserForm(FlaskForm):
    name = StringField('Nome', validators=[DataRequired()], description='Nome')
    username = StringField('Nome de usuário', validators=[DataRequired()], description='Nome  de usuário')
    email = StringField('email', validators=[DataRequired()], description='email')
    phone = StringField('Telefone', validators=[DataRequired()], description='Telefone')
    userRole = StringField('Tipo de usuário', validators=[DataRequired()], description='Tipo de usuário')
    address = StringField('Endereço', description='Tipo de usuário')
    # email_verify_token = StringField('email_verify_token', validators=[DataRequired()], description='Endereço')
    signupTerms = BooleanField('signupTerms', description='signupTerms')
    emailVerified = BooleanField('E-mail verificado?', description='E-mail verificado?')
